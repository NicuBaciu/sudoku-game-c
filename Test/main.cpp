#include <SFML/Graphics.hpp>
#include <time.h>
#include <fstream>
#include <iostream>
#include <stack>

#define w 40

using namespace sf;
using namespace std;

std::ifstream fin1("files/0.txt");
std::ifstream fin2("files/1.txt");
std::ifstream fin3("files/2.txt");
std::ifstream fin4("files/3.txt");
std::ifstream fin5("files/4.txt");

struct val
{
	int i;
	int j;
	int v;
};

std::ifstream fin_solved;
std::ofstream fout_solved;

std::ofstream fout;
std::ifstream fin;

std::ifstream fin_score;
std::ofstream fout_score;

short int unsolved1[10005][81];
short int unsolved2[10005][81];
short int unsolved3[10005][81];
short int unsolved4[10005][81];
short int unsolved5[10005][81];

bool issolved[10005];

bool fix[10][10];
int sudoku[10][10];

bool isfalse[10][10];

int score[6];

int nrdif, nrlvl[6];

std::stack <val> backtrack;

clock_t clock1;

void init_sudoku();
void load_sudoku(short int u[][81], int s[10][10], int x);
bool verify_sudoku(int x, int y);
void load_saved_sudoku(int dif, int lvl);
void add_button(int x, int y, int xtext, int ytext, String s, RenderWindow &app, int sx = 60, int sy = 25, double textscale = 0.7, bool isspecial = false);
void add_text(String s, int xtext, int ytext, RenderWindow &app, double textscale = 0.7);
void draw_app(RenderWindow &app, bool iscomplete, bool islvlok);

int auto_solve(int &i, int &j, std::stack <val> &s, int &ctval);
int a2solving();

void init_sudoku()
{
	std::string s;

	for(int i=0; i<10002; i++)
	{
		getline(fin1, s);

		for (int j = 0; j < s.size(); j++)
		{
			unsolved1[i][j] = s[j] - '0';
		}
	}

	for (int i = 0; i<10002; i++)
	{
		getline(fin2, s);

		for (int j = 0; j < s.size(); j++)
		{
			unsolved2[i][j] = s[j] - '0';
		}
	}

	for (int i = 0; i<10002; i++)
	{
		getline(fin3, s);

		for (int j = 0; j < s.size(); j++)
		{
			unsolved3[i][j] = s[j] - '0';
		}
	}

	for (int i = 0; i<10002; i++)
	{
		getline(fin4, s);

		for (int j = 0; j < s.size(); j++)
		{
			unsolved4[i][j] = s[j] - '0';
		}
	}

	for (int i = 0; i<10002; i++)
	{
		getline(fin5, s);

		for (int j = 0; j < s.size(); j++)
		{
			unsolved5[i][j] = s[j] - '0';
		}
	}

	fin.open("data/lvl.txt");

	fin >> nrdif;

	for (int i = 1; i <= 5; i++)
		fin >> nrlvl[i];

	fin.close();

	load_saved_sudoku(nrdif, nrlvl[nrdif]);

	if (nrdif == 1)
		fin_solved.open("data/dif1.txt");
	else if (nrdif == 2)
		fin_solved.open("data/dif2.txt");
	else if (nrdif == 3)
		fin_solved.open("data/dif3.txt");
	else if (nrdif == 4)
		fin_solved.open("data/dif4.txt");
	else if (nrdif == 5)
		fin_solved.open("data/dif5.txt");

	for (int i = 1; i<=10002; i++)
		fin_solved >> issolved[i];
		
	fin_solved.close();

	fin_score.open("data/score.txt");

	for (int i = 1; i <= 5; i++)
		fin_score >> score[i];

	fin_score.close();
}

void load_sudoku(short int u[][81], int s[10][10], int x)
{
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			if (u[x][i * 9 + j] == 0)
			{
				s[i + 1][j + 1] = -1;
				fix[i + 1][j + 1] = false;
			}
			else
			{
				s[i + 1][j + 1] = u[x][i * 9 + j] - 1;
				fix[i + 1][j + 1] = true;
			}
		}
	}
}

bool verify_sudoku(int x, int y)
{
	bool isok = true;

	for (int i = 1; i <= 9; i++)
	{
		if (sudoku[x][y] == sudoku[x][i] && i!=y)
			isok = false;
	}

	for (int i = 1; i <= 9; i++)
	{
		if (sudoku[x][y] == sudoku[i][y] && i!=x)
			isok = false;
	}

	
	if ((x-1) / 3 == 0 && (y - 1) / 3 == 0)
	{
		for (int i = 1; i <= 3; i++)
		{
			for (int j = 1; j <= 3; j++)
			{
				if (sudoku[x][y] == sudoku[i][j] && x != i && y != j)
					isok = false;
			}
		}
	}
	else if ((x - 1) / 3 == 1 && (y - 1) / 3 == 0)
	{
		for (int i = 4; i <= 6; i++)
		{
			for (int j = 1; j <= 3; j++)
			{
				if (sudoku[x][y] == sudoku[i][j] && x != i && y != j)
					isok = false;
			}
		}

	}
	else if ((x - 1) / 3 == 2 && (y - 1) / 3 == 0)
	{
		for (int i = 7; i <= 9; i++)
		{
			for (int j = 1; j <= 3; j++)
			{
				if (sudoku[x][y] == sudoku[i][j] && x != i && y != j)
					isok = false;
			}
		}
	}
	else if ((x - 1) / 3 == 0 && (y - 1) / 3 == 1)
	{
		for (int i = 1; i <= 3; i++)
		{
			for (int j = 4; j <= 6; j++)
			{
				if (sudoku[x][y] == sudoku[i][j] && x != i && y != j)
					isok = false;
			}
		}
	}
	else if ((x - 1) / 3 == 1 && (y - 1) / 3 == 1)
	{
		for (int i = 4; i <= 6; i++)
		{
			for (int j = 4; j <= 6; j++)
			{
				if (sudoku[x][y] == sudoku[i][j] && x != i && y != j)
					isok = false;
			}
		}
	}
	else if ((x - 1) / 3 == 2 && (y - 1) / 3 == 1)
	{
		for (int i = 7; i <= 9; i++)
		{
			for (int j = 4; j <= 6; j++)
			{
				if (sudoku[x][y] == sudoku[i][j] && x != i && y != j)
					isok = false;
			}
		}
	}
	else if ((x - 1) / 3 == 0 && (y - 1) / 3 == 2)
	{
		for (int i = 1; i <= 3; i++)
		{
			for (int j = 7; j <= 9; j++)
			{
				if (sudoku[x][y] == sudoku[i][j] && x != i && y != j)
					isok = false;
			}
		}
	}
	else if ((x - 1) / 3 == 1 && (y - 1) / 3 == 2)
	{
		for (int i = 4; i <= 6; i++)
		{
			for (int j = 7; j <= 9; j++)
			{
				if (sudoku[x][y] == sudoku[i][j] && x != i && y != j)
					isok = false;
			}
		}
	}
	else if ((x - 1) / 3 == 2 && (y-1) / 3 == 2)
	{
		for (int i = 7; i <= 9; i++)
		{
			for (int j = 7; j <= 9; j++)
			{
				if (sudoku[x][y] == sudoku[i][j] && x != i && y != j)
					isok = false;
			}
		}
	}

	return isok;

}

void load_saved_sudoku(int dif, int lvl)
{
	if (dif == 1)
	{
		load_sudoku(unsolved1, sudoku, lvl);
	}
	if (dif == 2)
	{
		load_sudoku(unsolved2, sudoku, lvl);
	}
	if (dif == 3)
	{
		load_sudoku(unsolved3, sudoku, lvl);
	}
	if (dif == 4)
	{
		load_sudoku(unsolved4, sudoku, lvl);
	}
	if (dif == 5)
	{
		load_sudoku(unsolved5, sudoku, lvl);
	}
}

void add_button(int x, int y, int xtext, int ytext, String s, RenderWindow &app, int sx , int sy , double textscale, bool isspecial )
{
	Font f1;

	f1.loadFromFile("fonts/Times_New_Roman_Normal.ttf");

	Text st1;

	st1.setString(s);
	st1.setFont(f1);
	st1.setScale(textscale, textscale);
	st1.setPosition(xtext, ytext);
	st1.setFillColor(Color::Black);

	RectangleShape test_btn;

	Color gray(150, 150, 150);
	Color greenish(100, 200, 0);

	test_btn.setPosition(x, y);
	test_btn.setSize(Vector2f(sx, sy));
	test_btn.setOutlineColor(Color::Black);
	if (!isspecial)
		test_btn.setFillColor(gray);
	else
		test_btn.setFillColor(greenish);
	test_btn.setOutlineThickness(1);

	app.draw(test_btn);
	app.draw(st1);
}

void add_text(String s, int xtext, int ytext, RenderWindow &app, double textscale )
{
	Font f1;

	f1.loadFromFile("fonts/Times_New_Roman_Normal.ttf");

	Text st1;

	st1.setString(s);
	st1.setFont(f1);
	st1.setScale(textscale, textscale);
	st1.setPosition(xtext, ytext);
	st1.setFillColor(Color::Black);

	app.draw(st1);
}

void draw_app(RenderWindow &app, bool iscomplete, bool islvlok)
{
	Texture t0, t1, t2;
	t0.loadFromFile("images/empty_grid2.png");
	t1.loadFromFile("images/num4.png");
	t2.loadFromFile("images/tick.png");
	Sprite s(t0);
	Sprite t(t1);
	Sprite isok(t2);

	Color background (250, 140 , 50);
	Color sudoku_bg(210, 200, 0);

	app.clear(background);

	RectangleShape sbg;

	sbg.setFillColor(sudoku_bg);
	sbg.setPosition(w, w);
	sbg.setSize(Vector2f(w*9, w*9));

	app.draw(sbg);

	if (iscomplete)
	{
		RectangleShape r;

		r.setOutlineColor(Color::Green);
		r.setFillColor(Color::Green);
		r.setPosition(w , w);
		r.setSize(Vector2f(w*9, w*9));

		app.draw(r);
	}

	if (islvlok)
	{
		isok.setPosition(204, -2);
		isok.scale(Vector2f(0.07, 0.07));
		app.draw(isok);
	}

	s.setPosition(w, w);
	app.draw(s);

	add_button(190, 420, 200, 420, "Test", app);

	add_button(280, 420, 285, 420, "Next Sudoku", app, 130);
	add_button(30, 420, 35, 420, "Prev Sudoku", app, 130);

	add_text("Select Difficulty: ", 30, 460, app);

	add_button(190, 460, 195, 460, "1", app, 20);
	add_button(220, 460, 225, 460, "2", app, 20);
	add_button(250, 460, 255, 460, "3", app, 20);
	add_button(280, 460, 285, 460, "4", app, 20);
	add_button(310, 460, 315, 460, "5", app, 20);

	std::string diftext;

	diftext = "Difficulty: " + std::to_string(nrdif) + "        Level: " + std::to_string(nrlvl[nrdif] + 1) + " / 10002";

	add_text(diftext, 80, 6, app);

	std::string scoretext;

	scoretext = "Your score is:";

	add_text("Your score: ", 20, 520, app);

	add_text("Dif 1", 145, 507, app, 0.5);
	add_text(std::to_string(score[1]), 135, 520, app);

	add_text("Dif 2", 205, 507, app, 0.5);
	add_text(std::to_string(score[2]), 195, 520, app);

	add_text("Dif 3", 265, 507, app, 0.5);
	add_text(std::to_string(score[3]), 255, 520, app);

	add_text("Dif 4", 325, 507, app, 0.5);
	add_text(std::to_string(score[4]), 315, 520, app);

	add_text("Dif 5", 385, 507, app, 0.5);
	add_text(std::to_string(score[5]), 375, 520, app);

	add_button(385, 6, 389, 6.5, "Auto", app, 47, 25, 0.6, true);
	add_button(410, 40, 412, 40, "A2", app, 27, 25, 0.6, true);
	add_button(10, 6, 14, 6.5, "Reset", app, 52, 25, 0.6, true);

	for (int i = 1; i <= 9; i++)
		for (int j = 1; j <= 9; j++)
		{
			if (isfalse[i][j])
			{
				RectangleShape r;

				r.setOutlineColor(Color::Red);
				r.setFillColor(Color::Red);
				r.setPosition(w*i + 1, w*j + 1);
				r.setSize(Vector2f(w, w));

				app.draw(r);
			}


			if (sudoku[i][j] != -1)
			{
				t.setTextureRect(IntRect((sudoku[i][j] % 3)*w, (sudoku[i][j] / 3)*w, w, w));
				t.setPosition(i*w, j*w);

				app.draw(t);
			}

		}

	app.display();
}

int auto_solve(int &i, int &j, std::stack <val> &s, int &ctval)
{
	//std::cout << (double)((double)((double)clock() - (double)clock1) / (double)CLOCKS_PER_SEC) << '\n';

	if (ctval < 8)
	{
		ctval++;
		sudoku[i][j] = ctval;
		//std::cout << "solving" << '\n';

		if (verify_sudoku(i, j))
		{
			val t;

			t.i = i;
			t.j = j;
			t.v = ctval;

			s.push(t);

			do
			{
				if (j < 9)
				{
					j++;
				}
				else
				{
					if (i == 9)
					{
						return 2;
					}
					else
					{
						i++;
						j = 1;
					}
				}
			} while (fix[i][j]);
		}
		else
			return 0;
	}
	else
	{
		sudoku[i][j] = -1;

		while (ctval == 8)
		{
			if (!s.empty())
			{
				val t;

				t = s.top(); s.pop();

				ctval = t.v;

				sudoku[t.i][t.j] = -1;

				i = t.i;
				j = t.j;
			}
			else
				return -1;
		}
		
		return 0;
	}

	return 1;
}



// ----------------- Tine de Auto2 ---------------- //

int g[100];
int a[100][100];
int gradnod[100];

struct nod
{
	int nr;
	int gr;
};

bool nod_sort(nod a, nod b)
{
	return a.gr > b.gr;
}

nod grade[100]; // Noduri libere cu gradele asociate
int nr_noduri_libere = 0;

void creeaza_graf()
{
	for (int i = 1; i <= 9; i++)
	{
		for (int j = 1; j <= 9; j++)
		{
			g[(i - 1) * 9 + j] = sudoku[i][j] + 1;
		}
	}

	//for (int i = 1; i <= 81; i++)
	//	g[i] = sud[i - 1] - '0';

	for (int i = 1; i <= 81; i++)
		for (int j = 1; j <= 81; j++)
			a[i][j] = 0;

	for (int i = 1; i <= 81; i++)
		gradnod[i] = 0;

	// ------------------ ORIZONTAL SI VERTICAL --------------------------- //

	for (int i = 1; i <= 81; i++)
	{
		for (int j = i + 1; j <= 81; j++)
		{
			bool ok = false;

			if (i != j)
			{
				if (i % 9 == j % 9)
				{
					ok = true;
				}
				else if (j >= i / 9 && j <= (i / 9 + 1) * 9)
				{
					if (i % 9 != 0)
						ok = true;
				}

				if (ok)
				{

					a[i][j] = 1;
					a[j][i] = 1;

					if (g[i] == 0 && g[j] != 0)
						gradnod[i]++;
					if (g[j] == 0 && g[i] != 0)
						gradnod[j]++;
				}
			}
		}
	}

	// ----------------- CASETA --------------------- //

	for (int i0 = 0; i0 <= 2; i0++)
	{
		for (int j0 = 0; j0 <= 2; j0++)
		{
			int cti, ctj;
			int inext, jnext;

			int x, y;

			// SUS STANGA //

			cti = i0 * 3 + 1;
			ctj = j0 * 3 + 1;

			x = (cti - 1) * 9 + ctj;

			for (int k = 1; k <= 2; k++)
			{
				for (int p = 1; p <= 2; p++)
				{
					y = (cti + k - 1) * 9 + ctj + p;

					a[x][y] = 1;
					a[y][x] = 1;

					if (g[x] == 0 && g[y] != 0)
						gradnod[x]++;
					if (g[y] == 0 && g[x] != 0)
						gradnod[y]++;
				}
			}

			// SUS MIJLOC //

			cti = i0 * 3 + 1;
			ctj = j0 * 3 + 2;

			x = (cti - 1) * 9 + ctj;

			for (int k = 1; k <= 2; k++)
			{
				y = (cti + k - 1) * 9 + ctj - 1;

				a[x][y] = 1;
				a[y][x] = 1;

				if (g[x] == 0 && g[y] != 0)
					gradnod[x]++;
				if (g[y] == 0 && g[x] != 0)
					gradnod[y]++;

				y = (cti + k - 1) * 9 + ctj + 1;

				a[x][y] = 1;
				a[y][x] = 1;

				if (g[x] == 0 && g[y] != 0)
					gradnod[x]++;
				if (g[y] == 0 && g[x] != 0)
					gradnod[y]++;
			}

			// SUS DREAPTA //

			cti = i0 * 3 + 1;
			ctj = j0 * 3 + 3;

			x = (cti - 1) * 9 + ctj;

			for (int k = 1; k <= 2; k++)
			{
				for (int p = 1; p <= 2; p++)
				{
					y = (cti + k - 1) * 9 + ctj - p;

					a[x][y] = 1;
					a[y][x] = 1;

					if (g[x] == 0 && g[y] != 0)
						gradnod[x]++;
					if (g[y] == 0 && g[x] != 0)
						gradnod[y]++;
				}
			}



			// MUJLOC STANGA //

			cti = i0 * 3 + 2;
			ctj = j0 * 3 + 1;

			x = (cti - 1) * 9 + ctj;

			for (int p = 1; p <= 2; p++)
			{
				y = cti * 9 + ctj + p;

				a[x][y] = 1;
				a[y][x] = 1;

				if (g[x] == 0 && g[y] != 0)
					gradnod[x]++;
				if (g[y] == 0 && g[x] != 0)
					gradnod[y]++;
			}


			// MIJLOC MIJLOC //

			cti = i0 * 3 + 2;
			ctj = j0 * 3 + 2;

			x = (cti - 1) * 9 + ctj;

			y = cti * 9 + ctj - 1;

			a[x][y] = 1;
			a[y][x] = 1;

			if (g[x] == 0 && g[y] != 0)
				gradnod[x]++;
			if (g[y] == 0 && g[x] != 0)
				gradnod[y]++;


			y = cti * 9 + ctj + 1;

			a[x][y] = 1;
			a[y][x] = 1;

			if (g[x] == 0 && g[y] != 0)
				gradnod[x]++;
			if (g[y] == 0 && g[x] != 0)
				gradnod[y]++;

			// MIJLOC DREAPTA //

			cti = i0 * 3 + 2;
			ctj = j0 * 3 + 3;

			x = (cti - 1) * 9 + ctj;

			for (int p = 1; p <= 2; p++)
			{
				y = cti * 9 + ctj - p;

				a[x][y] = 1;
				a[y][x] = 1;

				if (g[x] == 0 && g[y] != 0)
					gradnod[x]++;
				if (g[y] == 0 && g[x] != 0)
					gradnod[y]++;
			}
		}
	}
}

int a2solving()
{
	sort(grade + 1, grade + nr_noduri_libere, nod_sort); // sortare vector noduri libere + grade

	int colors[10]; // culori disponibile pt pozitia curenta
	int nr_col = 9; // nr cul. disponibile

	for (int i = 1; i <= 10; i++) // init culori
		colors[i] = 0;

	int nr_grad_max = 0; // nr pozitiei in grade a nodului cu grad maxim care poate fi determinat clar

	while (nr_col > 1) // cat timp exista mai mult de o posibilitate de completare a pozitiei curente
	{
		nr_grad_max++;

		if (nr_grad_max > nr_noduri_libere) // daca a depasit numarul de noduri libere si nu s-a gasit niciunul care poate fi completat sigur
		{
			//cout << "Not Solvable like this" << '\n';
			return -1;
		}

		nr_col = 9; // reinitializarea numarului de culori

		for (int i = 1; i <= 10; i++) // reinit vector culori
			colors[i] = 0;

		for (int i = 1; i <= 81; i++) // parcurgerea vecinilor nodului curent in graf
		{
			if (a[grade[nr_grad_max].nr][i] != 0 && g[i] != 0) // verificare daca sunt vecini si daca nu sunt celule libere
			{
				if (colors[g[i]] == 0) // ferificare daca a aparut o noua culoare
				{
					colors[g[i]] = 1; // marcarea culorii noi aparute
					nr_col--; // decrementarea numarului de culori disponibile
				}
				if (nr_col <= 1) // daca se poate determina culoare cu exactitate
					break;
			}
		}
	}

	int color; // culoarea de umplere a celulei curente

	cout << grade[nr_grad_max].nr << " " << nr_col << '\n';

	for (int i = 1; i <= 9; i++) // determinarea culorii de umplere prin
		if (colors[i] == 0) // care pozitie contine valoarea 0 -> culoarea lipsa care trebuie completata in celula curenta
			color = i;

	g[grade[nr_grad_max].nr] = color; // completarea celulei

	int pos = grade[nr_grad_max].nr;

	if (pos % 9 != 0)
	{
		sudoku[pos / 9 + 1][pos % 9] = color - 1;
	}
	else if (pos % 9 == 0)
	{
		sudoku[pos / 9][9] = color - 1;
	}

	for (int j = 1; j <= 81; j++) // recalcularea gradelor nodurilor afectate de schimbarea culorii celulei curente
	{                            // incrementarea gradului pentru nodurile adiacente libere
		if (a[pos][j] == 1 && g[j] == 0)
			gradnod[j]++;
	}

	nr_noduri_libere--; // decrementarea nr de noduri libere

	int k = 0;

	for (int i = 1; i <= 81; i++) // reinitializarea vectorului grade
	{
		if (g[i] == 0)
		{
			k++;

			grade[k].gr = gradnod[i];
			grade[k].nr = i;
		}
	}

	return nr_noduri_libere;
}

// ----------------------------------------------- //

int main()
{
	srand(time(0));

	RenderWindow app(VideoMode(440, 570), "Sudoku!");

	init_sudoku();

	int xclick=0, yclick=0;
	bool isfixed = false;
	bool iscomplete = false;

	for (int i = 1; i <= 9; i++)
	{
		for (int j = 1; j <= 9; j++)
		{
			isfalse[i][j] = false;
		}
	}

	bool isautosolved = false;

	bool isa2solving = false;

	bool issolving = false;
	int ctval = -1;
	int i = 1, j = 1;
	int cti = 1;
	int ctj = 1;

	while (app.isOpen())
	{
		Vector2i pos = Mouse::getPosition(app);
		int x = pos.x / w;
		int y = pos.y / w;

		Event e;

		bool no_backtrack = false;
		bool sem = false;
		int ret;

		if (issolving)
		{
			if (!fix[i][j])
			{
				ret = auto_solve(i, j, backtrack, ctval);
				clock1 = clock();

				if (ret == 1)
					ctval = -1;
				else if (ret == -1)
				{
					do
					{
						if (ctj < 9)
							ctj++;
						else
						{
							cti++;
							ctj = 0;
						}
					} while (fix[cti][ctj]);

					i = cti;
					j = ctj;
				}
				else if (ret == 2)
				{
					issolving = false;

					cti = 1;
					ctj = 1;
					i = 1;
					j = 1;

					ctval = -1;

					std::cout << "Solved!!!!" << '\n';

					int ct = 0;

					for (int i = 1; i <= 9; i++)
					{
						for (int j = 1; j <= 9; j++)
						{
							if (verify_sudoku(i, j) == 0 && sudoku[i][j] != -1 && fix[i][j] == false)
							{
								isfalse[i][j] = true;
							}
							else
								isfalse[i][j] = false;

							if (isfalse[i][j] == false && sudoku[i][j] != -1)
							{
								ct++;
							}
						}
					}

					if (ct == 81)
					{
						iscomplete = true;
					}
				}
			}
			else
			{
				do
				{
					if (j < 9)
						j++;
					else
					{
						i++;
						j = 0;
					}
				} while (fix[i][j] || (i<=9 && j<=9));
			}
		}

		if (isa2solving)
		{
			int ret = a2solving();

			if (ret == 0)
			{
				isa2solving = false;
				iscomplete = true;
			}
			if (ret == -1)
			{
				cout << "Nu se poate rezolva prin metoda aceasta!" << '\n';

				isa2solving = false;
			}
		}

		while (app.pollEvent(e))
		{
			if (e.type == Event::Closed)
			{
				app.close();

				fout.open("data/lvl.txt");

				fout << nrdif << " ";
				
				for(int i=1; i<=5; i++)
					fout << nrlvl[i] << " ";

				fout.close();

				if (nrdif == 1)
					fout_solved.open("data/dif1.txt");
				else if (nrdif == 2)
					fout_solved.open("data/dif2.txt");
				else if (nrdif == 3)
					fout_solved.open("data/dif3.txt");
				else if (nrdif == 4)
					fout_solved.open("data/dif4.txt");
				else if (nrdif == 5)
					fout_solved.open("data/dif5.txt");


				for (int i = 1; i <= 10002; i++)
					fout_solved << issolved[i] << " ";

				fout_solved.close();

				fout_score.open("data/score.txt");

				for (int i = 1; i <= 5; i++)
					fout_score << score[i] << " ";

				fout_score.close();
			}
				

			if (e.type == Event::MouseButtonPressed)
			{
				if (e.key.code == Mouse::Left)
				{
					if (pos.x > w && pos.y > w && pos.x < w * 10 && pos.y < w * 10) // Click pe tabla de joc
					{
						for (int i = 1; i <= 9; i++)
						{
							for (int j = 1; j <= 9; j++)
							{
								isfalse[i][j] = false;
							}
						}

						xclick = x;
						yclick = y;

						if (fix[xclick][yclick])
						{
							isfixed = true;
						}
						else
						{
							isfixed = false;
						}
					}
					else if (pos.x > 190 && pos.x < 250 && pos.y > 420 && pos.y < 445) // Test Click
					{
						int ct = 0;

						for (int i = 1; i <= 9; i++)
						{
							for (int j = 1; j <= 9; j++)
							{
								if (verify_sudoku(i, j) == 0 && sudoku[i][j] != -1 && fix[i][j] == false)
								{
									isfalse[i][j] = true;
								}
								else
									isfalse[i][j] = false;

								if (isfalse[i][j] == false && sudoku[i][j] != -1)
								{
									ct++;
								}
							}
						}

						if (ct == 81)
						{
							iscomplete = true;

							if (issolved[nrlvl[nrdif]+1] == false && !isautosolved)
							{
								issolved[nrlvl[nrdif]+1] = true;
								score[nrdif]++;
							}
						}
					}
					else if (pos.x > 280 && pos.x < 410 && pos.y > 420 && pos.y < 445) // Next Level Click
					{
						for (int i = 1; i <= 9; i++)
						{
							for (int j = 1; j <= 9; j++)
							{
								isfalse[i][j] = false;
							}
						}

						if (nrlvl[nrdif] < 10002)
						{
							nrlvl[nrdif]++;
							load_saved_sudoku(nrdif, nrlvl[nrdif]);
							iscomplete = false;
						}
					}
					else if (pos.x > 30 && pos.x < 160 && pos.y > 420 && pos.y < 445) // Prev Level Click
					{
						for (int i = 1; i <= 9; i++)
						{
							for (int j = 1; j <= 9; j++)
							{
								isfalse[i][j] = false;
							}
						}

						if (nrlvl[nrdif] > 0)
						{
							nrlvl[nrdif]--;
							load_saved_sudoku(nrdif, nrlvl[nrdif]);
							iscomplete = false;
						}
					}
					else if (pos.x > 190 && pos.x < 210 && pos.y > 460 && pos.y < 485) // Dif 1
					{
						for (int i = 1; i <= 9; i++)
						{
							for (int j = 1; j <= 9; j++)
							{
								isfalse[i][j] = false;
							}
						}

						iscomplete = false;

						if (nrdif == 1)
							fout_solved.open("data/dif1.txt");
						else if (nrdif == 2)
							fout_solved.open("data/dif2.txt");
						else if (nrdif == 3)
							fout_solved.open("data/dif3.txt");
						else if (nrdif == 4)
							fout_solved.open("data/dif4.txt");
						else if (nrdif == 5)
							fout_solved.open("data/dif5.txt");


						for (int i = 1; i <= 10002; i++)
							fout_solved << issolved[i] << " ";

						fout_solved.close();

						nrdif = 1;


						load_saved_sudoku(nrdif, nrlvl[nrdif]);


						fin_solved.open("data/dif1.txt");

						for (int i = 1; i <= 10002; i++)
							fin_solved >> issolved[i];

						fin_solved.close();
					}
					else if (pos.x > 220 && pos.x < 240 && pos.y > 460 && pos.y < 485) // Dif 2
					{
						for (int i = 1; i <= 9; i++)
						{
							for (int j = 1; j <= 9; j++)
							{
								isfalse[i][j] = false;
							}
						}

						iscomplete = false;

						if (nrdif == 1)
							fout_solved.open("data/dif1.txt");
						else if (nrdif == 2)
							fout_solved.open("data/dif2.txt");
						else if (nrdif == 3)
							fout_solved.open("data/dif3.txt");
						else if (nrdif == 4)
							fout_solved.open("data/dif4.txt");
						else if (nrdif == 5)
							fout_solved.open("data/dif5.txt");


						for (int i = 1; i <= 10002; i++)
							fout_solved << issolved[i] << " ";

						fout_solved.close();

						nrdif = 2;


						load_saved_sudoku(nrdif, nrlvl[nrdif]);


						fin_solved.open("data/dif2.txt");

						for (int i = 1; i <= 10002; i++)
							fin_solved >> issolved[i];

						fin_solved.close();
					}
					else if (pos.x > 250 && pos.x < 270 && pos.y > 460 && pos.y < 485) // Dif 3
					{
						for (int i = 1; i <= 9; i++)
						{
							for (int j = 1; j <= 9; j++)
							{
								isfalse[i][j] = false;
							}
						}

						iscomplete = false;

						if (nrdif == 1)
							fout_solved.open("data/dif1.txt");
						else if (nrdif == 2)
							fout_solved.open("data/dif2.txt");
						else if (nrdif == 3)
							fout_solved.open("data/dif3.txt");
						else if (nrdif == 4)
							fout_solved.open("data/dif4.txt");
						else if (nrdif == 5)
							fout_solved.open("data/dif5.txt");


						for (int i = 1; i <= 10002; i++)
							fout_solved << issolved[i] << " ";

						fout_solved.close();

						nrdif = 3;


						load_saved_sudoku(nrdif, nrlvl[nrdif]);


						fin_solved.open("data/dif3.txt");

						for (int i = 1; i <= 10002; i++)
							fin_solved >> issolved[i];

						fin_solved.close();
					}
					else if (pos.x > 280 && pos.x < 300 && pos.y > 460 && pos.y < 485) // Dif 4
					{
						for (int i = 1; i <= 9; i++)
						{
							for (int j = 1; j <= 9; j++)
							{
								isfalse[i][j] = false;
							}
						}

						iscomplete = false;

						if (nrdif == 1)
							fout_solved.open("data/dif1.txt");
						else if (nrdif == 2)
							fout_solved.open("data/dif2.txt");
						else if (nrdif == 3)
							fout_solved.open("data/dif3.txt");
						else if (nrdif == 4)
							fout_solved.open("data/dif4.txt");
						else if (nrdif == 5)
							fout_solved.open("data/dif5.txt");


						for (int i = 1; i <= 10002; i++)
							fout_solved << issolved[i] << " ";

						fout_solved.close();

						nrdif = 4;


						load_saved_sudoku(nrdif, nrlvl[nrdif]);


						fin_solved.open("data/dif4.txt");

						for (int i = 1; i <= 10002; i++)
							fin_solved >> issolved[i];

						fin_solved.close();
					}
					else if (pos.x > 310 && pos.x < 330 && pos.y > 460 && pos.y < 485) // Dif 5
					{
						for (int i = 1; i <= 9; i++)
						{
							for (int j = 1; j <= 9; j++)
							{
								isfalse[i][j] = false;
							}
						}

						iscomplete = false;

						if (nrdif == 1)
							fout_solved.open("data/dif1.txt");
						else if (nrdif == 2)
							fout_solved.open("data/dif2.txt");
						else if (nrdif == 3)
							fout_solved.open("data/dif3.txt");
						else if (nrdif == 4)
							fout_solved.open("data/dif4.txt");
						else if (nrdif == 5)
							fout_solved.open("data/dif5.txt");


						for (int i = 1; i <= 10002; i++)
							fout_solved << issolved[i] << " ";

						fout_solved.close();

						nrdif = 5;


						load_saved_sudoku(nrdif, nrlvl[nrdif]);


						fin_solved.open("data/dif5.txt");

						for (int i = 1; i <= 10002; i++)
							fin_solved >> issolved[i];

						fin_solved.close();
					}
					else if (pos.x > 385 && pos.x < 432 && pos.y > 6 && pos.y < 31) // Auto Click
					{
						for (int i = 1; i <= 9; i++)
						{
							for (int j = 1; j <= 9; j++)
							{
								isfalse[i][j] = false;
							}
						}

						std::cout << "auto \n" ;

						isautosolved = true;

						issolving = true;
					}
					else if (pos.x > 410 && pos.x < 437 && pos.y > 40 && pos.y < 65) // A2 Click
					{
						isa2solving = true;

						std::cout << "auto2 \n";

						creeaza_graf();

						for (int i = 1; i <= 81; i++) // initializare vector grade + numarare noduri libere
						{
							if (g[i] == 0)
							{
								nr_noduri_libere++;
								grade[nr_noduri_libere].gr = gradnod[i];
								grade[nr_noduri_libere].nr = i;
							}
						}

						isautosolved = true;
					}
					else if (pos.x > 10 && pos.x < 62 && pos.y > 6 && pos.y < 31) // Reset Click
					{
						for (int i = 1; i <= 9; i++)
						{
							for (int j = 1; j <= 9; j++)
							{
								isfalse[i][j] = false;
							}
						}

						std::cout << "reset \n";

						isautosolved = false;
						iscomplete = false;
						issolving = false;

						for (int i = 1; i <= 9; i++)
							for (int j = 1; j <= 9; j++)
								if (fix[i][j] == false)
									sudoku[i][j] = -1;
					}
				}
				if (e.key.code == Mouse::Right)
				{
					if (fix[x][y] == false)
						sudoku[x][y] = -1;
				}
			}

			if (!isfixed)
			{
				if (e.type == Event::EventType::KeyPressed)
				{
					if (e.key.code == Keyboard::Num1)
					{
						sudoku[xclick][yclick] = 0;
					}
					if (e.key.code == Keyboard::Num2)
					{
						sudoku[xclick][yclick] = 1;
					}
					if (e.key.code == Keyboard::Num3)
					{
						sudoku[xclick][yclick] = 2;
					}
					if (e.key.code == Keyboard::Num4)
					{
						sudoku[xclick][yclick] = 3;
					}
					if (e.key.code == Keyboard::Num5)
					{
						sudoku[xclick][yclick] = 4;
					}
					if (e.key.code == Keyboard::Num6)
					{
						sudoku[xclick][yclick] = 5;
					}
					if (e.key.code == Keyboard::Num7)
					{
						sudoku[xclick][yclick] = 6;
					}
					if (e.key.code == Keyboard::Num8)
					{
						sudoku[xclick][yclick] = 7;
					}
					if (e.key.code == Keyboard::Num9)
					{
						sudoku[xclick][yclick] = 8;
					}
				}
			}
		}

		draw_app(app, iscomplete, issolved[nrlvl[nrdif]+1]);
	}

	return 0;
}
